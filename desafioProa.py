from collections import Counter

frase = input("Digite sua frase: ") #PROA Tecnologia Criativa
frase = frase.replace(" ","")

counter = Counter(frase)
print(counter)


#Format em Python 3.5
# print(f'A frase é: {frase}')


# #Exemplos de Counter
# c = Counter(a=3, b=1)
# d = Counter(a=1, b=2)
#
# #adição
# print(c + d)
#
# # subtração
# print(c - d)
#
# #pega o min(c[x], d[x])
# print(c & d)
#
# #pega o max(c[x], d[x])
# print(c | d)
